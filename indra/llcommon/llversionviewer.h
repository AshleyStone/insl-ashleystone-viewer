/** 
 * @file llversionviewer.h
 * @brief
 *
 * $LicenseInfo:firstyear=2002&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#ifndef LL_LLVERSIONVIEWER_H
#define LL_LLVERSIONVIEWER_H

// Note: This viewer is based on 3.3.3, somewhere around there. :)
// Todo: Build strings from S32's rather than editing all four/three sets?

const S32 LL_VERSION_MAJOR = 12;
const S32 LL_VERSION_MINOR = 8;
const S32 LL_VERSION_PATCH = 9;
const S32 LL_VERSION_BUILD = 1;

const char * const EXO_VERSION_STRING = "12.08.09.1";
const char * const EXO_VERSION_STRING_SHORT = "12.08.09";

#ifdef LL_WINDOWS
const char * const EXO_AGENT_VERSION = "Win/12.08.09.1";
#else
#ifdef LL_DARWIN
const char * const EXO_AGENT_VERSION = "Mac/12.08.09.1";
#else
const char * const EXO_AGENT_VERSION = "Lin/12.08.09.1";
#endif
#endif

const char * const LL_CHANNEL = "Exodus Viewer Beta";

#if LL_DARWIN
const char * const LL_VERSION_BUNDLE_ID = "com.exodusviewer.viewer";
#endif

#endif // LL_LLVERSIONVIEWER_H
