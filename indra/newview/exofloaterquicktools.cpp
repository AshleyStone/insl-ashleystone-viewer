/** 
 * @file exofloaterquicktools.cpp
 * @brief The advanced graphics window for adjusting advanced shaders.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "message.h"

#include "llfloaterreg.h"
#include "exofloaterquicktools.h"

#include "llcombobox.h"
#include "llviewercontrol.h"
#include "llwlparamset.h"
#include "llwlparammanager.h"
#include "llwaterparammanager.h"

// exodus_floater_quick_tools.xml

exoFloaterQuickTools::exoFloaterQuickTools(const LLSD& key) :
	LLTransientDockableFloater(NULL, true, key)
{
}

exoFloaterQuickTools::~exoFloaterQuickTools()
{
}

BOOL exoFloaterQuickTools::postBuild()
{
	LLEnvManagerNew& env_mgr = LLEnvManagerNew::instance();
	
	LLComboBox* skyComboBox = getChild<LLComboBox>("SkyPresetsCombo");
	if (skyComboBox)
	{
		skyComboBox->setCommitCallback(boost::bind(&exoFloaterQuickTools::onChangeSkyPreset, this, _1));

		LLWLParamManager::preset_name_list_t user_presets, sys_presets, region_presets;
		LLWLParamManager::instance().getPresetNames(region_presets, user_presets, sys_presets);

		for(LLWLParamManager::preset_name_list_t::const_iterator it = sys_presets.begin(); it != sys_presets.end(); ++it)
		{
			if ((*it).length() > 0) skyComboBox->add(*it);
		}

		skyComboBox->addSeparator();

		for(LLWLParamManager::preset_name_list_t::const_iterator it = user_presets.begin(); it != user_presets.end(); ++it)
		{
			if ((*it).length() > 0) skyComboBox->add(*it);
		}

		skyComboBox->selectByValue(env_mgr.getSkyPresetName());
	}

	LLComboBox* waterComboBox = getChild<LLComboBox>("WaterPresetsCombo");
	if (waterComboBox)
	{
		waterComboBox->setCommitCallback(boost::bind(&exoFloaterQuickTools::onChangeWaterPreset, this, _1));

		std::list<std::string> user_presets, system_presets;
		LLWaterParamManager::instance().getPresetNames(user_presets, system_presets);
		
		for(std::list<std::string>::const_iterator mIt = system_presets.begin(); mIt != system_presets.end(); ++mIt)
		{
			if ((*mIt).length() > 0) waterComboBox->add(*mIt);
		}
		
		waterComboBox->addSeparator();

		for(std::list<std::string>::const_iterator mIt = user_presets.begin(); mIt != user_presets.end(); ++mIt)
		{
			if ((*mIt).length() > 0) waterComboBox->add(*mIt);
		}

		waterComboBox->selectByValue(env_mgr.getWaterPresetName());
	}
	
	getChild<LLUICtrl>("SkyPrevPreset")->setCommitCallback(boost::bind(&exoFloaterQuickTools::onClickSkyPrev, this));
	getChild<LLUICtrl>("SkyNextPreset")->setCommitCallback(boost::bind(&exoFloaterQuickTools::onClickSkyNext, this));
	
	getChild<LLUICtrl>("WaterPrevPreset")->setCommitCallback(boost::bind(&exoFloaterQuickTools::onClickWaterPrev, this));
	getChild<LLUICtrl>("WaterNextPreset")->setCommitCallback(boost::bind(&exoFloaterQuickTools::onClickWaterNext, this));

	return LLDockableFloater::postBuild();
}

void exoFloaterQuickTools::onOpen(const LLSD& key)
{
	/*LLButton* button = LLBottomTray::instance().getChild<LLButton>("quick_tools_btn");
	if (button) setDockControl(new LLDockControl(button, this, getDockTongue(), LLDockControl::TOP));*/
}

void exoFloaterQuickTools::onChangeSkyPreset(LLUICtrl* ctrl)
{
	std::string data = ctrl->getValue().asString();
	if (!data.empty()) LLEnvManagerNew::instance().setUseSkyPreset(data, gSavedSettings.getBOOL("ExodusQuickToolsInterpolateWL"));
}

void exoFloaterQuickTools::onClickSkyPrev()
{
	LLComboBox* comboBox = getChild<LLComboBox>("SkyPresetsCombo");
	if (comboBox)
	{
		int previousIndex = comboBox->getCurrentIndex() - 1;
		int lastItem = comboBox->getItemCount() - 1;
		if (previousIndex < 0) previousIndex = lastItem;
		else if (previousIndex > lastItem) previousIndex = 0;
		
		if (!comboBox->setCurrentByIndex(previousIndex)) // Because of the separator, bleh.
		{
			previousIndex -= 1;
			comboBox->setCurrentByIndex(previousIndex);
		}
		
		LLEnvManagerNew::instance().setUseSkyPreset(comboBox->getValue().asString(), gSavedSettings.getBOOL("ExodusQuickToolsInterpolateWL"));
	}
}

void exoFloaterQuickTools::onClickSkyNext()
{
	LLComboBox* comboBox = getChild<LLComboBox>("SkyPresetsCombo");
	if (comboBox)
	{
		int nextIndex = comboBox->getCurrentIndex() + 1;
		int lastItem = comboBox->getItemCount() - 1;
		if (nextIndex < 0) nextIndex = lastItem;
		else if (nextIndex > lastItem) nextIndex = 0;

		if (!comboBox->setCurrentByIndex(nextIndex)) // Because of the separator, bleh.
		{
			nextIndex += 1;
			comboBox->setCurrentByIndex(nextIndex);
		}

		LLEnvManagerNew::instance().setUseSkyPreset(comboBox->getValue().asString(), gSavedSettings.getBOOL("ExodusQuickToolsInterpolateWL"));
	}
}

void exoFloaterQuickTools::onChangeWaterPreset(LLUICtrl* ctrl)
{
	std::string data = ctrl->getValue().asString();
	if (!data.empty()) LLEnvManagerNew::instance().setUseWaterPreset(data, gSavedSettings.getBOOL("ExodusQuickToolsInterpolateWL"));
}

void exoFloaterQuickTools::onClickWaterPrev()
{
	LLComboBox* comboBox = getChild<LLComboBox>("WaterPresetsCombo");
	if (comboBox)
	{
		int previousIndex = comboBox->getCurrentIndex() - 1;
		int lastItem = comboBox->getItemCount() - 1;
		if (previousIndex < 0) previousIndex = lastItem;
		else if (previousIndex > lastItem) previousIndex = 0;
		
		if (!comboBox->setCurrentByIndex(previousIndex)) // Because of the separator, bleh.
		{
			previousIndex -= 1;
			comboBox->setCurrentByIndex(previousIndex);
		}

		LLEnvManagerNew::instance().setUseWaterPreset(comboBox->getValue().asString(), gSavedSettings.getBOOL("ExodusQuickToolsInterpolateWL"));
	}
}

void exoFloaterQuickTools::onClickWaterNext()
{
	LLComboBox* comboBox = getChild<LLComboBox>("WaterPresetsCombo");
	if (comboBox)
	{
		int nextIndex = comboBox->getCurrentIndex() + 1;
		int lastItem = comboBox->getItemCount() - 1;
		if (nextIndex < 0) nextIndex = lastItem;
		else if (nextIndex > lastItem) nextIndex = 0;

		if (!comboBox->setCurrentByIndex(nextIndex)) // Because of the separator, bleh.
		{
			nextIndex += 1;
			comboBox->setCurrentByIndex(nextIndex);
		}

		LLEnvManagerNew::instance().setUseWaterPreset(comboBox->getValue().asString(), gSavedSettings.getBOOL("ExodusQuickToolsInterpolateWL"));
	}
}
