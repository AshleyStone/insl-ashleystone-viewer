/** 
 * @file exoinventoryhotkeys.cpp
 * @brief Manages inventory bindings and hotkeys.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "exoinventoryhotkeys.h"

#include "llagent.h"
#include "llagentwearables.h"
#include "llappearancemgr.h"
#include "llfloaterreg.h"
#include "llgesturemgr.h"
#include "llkeyboard.h"
#include "llmodaldialog.h"
#include "llnotificationsutil.h"
#include "llinventorybridge.h"
#include "llviewercontrol.h"
#include "llviewerinventory.h"
#include "llvoavatarself.h"

exoInventoryHotkeys* exoInventoryHotkeys::sInstance = NULL;

// exo_floater_inventory_hotkeys.xml

exoBindedInventory::exoBindedInventory() :
	mKey(),
	mLabel(),
	mMask(),
	mItem(),
	mReplace()
{
}

exoBindedInventory::exoBindedInventory(LLUUID id, std::string label, KEY key, MASK mask, bool replace) :
	mKey(key),
	mLabel(label),
	mMask(mask),
	mItem(id),
	mReplace(replace)
{
}
	
class exoBindingSetKey : public LLModalDialog
{
public:
	exoBindingSetKey(const LLSD& key);
	~exoBindingSetKey(){};
	
	BOOL postBuild();

	void setStuff(exoInventoryHotkeys* parent, LLInventoryItem* item){ mParent = parent; mItem = item; }

	BOOL handleKeyHere(KEY key, MASK mask);
	static void onCancel(void* user_data);
		
private:
	exoInventoryHotkeys* mParent;
	LLInventoryItem* mItem;
};

exoBindingSetKey::exoBindingSetKey(const LLSD& key) : LLModalDialog(key), mParent(NULL)
{
}

BOOL exoBindingSetKey::postBuild()
{
	childSetAction("Cancel", onCancel, this);
	getChild<LLUICtrl>("Cancel")->setFocus(TRUE);
	
	gFocusMgr.setKeystrokesOnly(TRUE);
	
	return TRUE;
}

BOOL exoBindingSetKey::handleKeyHere(KEY key, MASK mask)
{
	if ((key == 'Q' && mask == MASK_CONTROL) || key == KEY_ESCAPE ||
		key == 'W' || key == 'A' || key == 'S' || key == 'D' || key == 'Q' || key == 'Z' ||
		key == KEY_UP || key == KEY_LEFT || key == KEY_DOWN || key == KEY_RIGHT ||
		key == 'E' || key == KEY_PAGE_UP)
	{
		return FALSE;
	}
	else if (mParent)
	{
		mParent->bindInventory(mItem, key, mask);
		closeFloater();

		return TRUE;
	}
	else return FALSE;
}

void exoBindingSetKey::onCancel(void* user_data)
{
	exoBindingSetKey* self = (exoBindingSetKey*)user_data;
	self->closeFloater();
}

exoInventoryHotkeys::exoInventoryHotkeys(const LLSD& key) :
	LLFloater(key)
{
	LLFloaterReg::add("inventory_binding_key", "exo_inventory_binding_key.xml", (LLFloaterBuildFunc)&LLFloaterReg::build<exoBindingSetKey>);
}

exoInventoryHotkeys::~exoInventoryHotkeys()
{
	sInstance = NULL;
}
	
exoInventoryHotkeys* exoInventoryHotkeys::getInstance()
{
	if (!sInstance) sInstance = LLFloaterReg::getTypedInstance<exoInventoryHotkeys>("exo_inventory_hotkeys");
	return sInstance;
}

void exoInventoryHotkeys::draw()
{
	if (getVisible()) LLFloater::draw();
}

BOOL exoInventoryHotkeys::initBindings()
{
	// Todo: Load bindings from EVON file.

	return TRUE;
}

BOOL exoInventoryHotkeys::postBuild()
{
	// Todo: Lots of shit, like for UI preferences and stuff.
	// Todo: Not sure what else, maybe settings and stuff?

	setVisible(FALSE);
	initBindings();

	// ...

	return TRUE;
}

std::string exoInventoryHotkeys::getLabel(LLUUID id)
{
	// Todo: Maybe use strings.xml for future translations?

	return mBindings.count(id) ? 
		(" (bound to " + LLKeyboard::stringFromAccelerator(mBindings[id]->mMask, mBindings[id]->mKey)) + ")" : "";
}

void exoInventoryHotkeys::bindInventory(LLInventoryItem* item)
{
	exoBindingSetKey* dialog = LLFloaterReg::showTypedInstance<exoBindingSetKey>("inventory_binding_key", item->getUUID(), TRUE);
	if (dialog)
	{
		dialog->setStuff(this, item);
		dialog->center();
	}
}

void exoInventoryHotkeys::bindInventory(LLInventoryItem* item, KEY key, MASK mask)
{
	// Todo: Allow user to adjust label.

	bindInventory(item, item->getName(), key, mask);
}

void exoInventoryHotkeys::bindInventory(LLInventoryItem* item, std::string label, KEY key, MASK mask)
{
	// Todo: Various checks on inventory type, maybe?
	// Todo: Check if item is already binded on the same combo?
	// Todo: Notify user that that key binding is binded by something else, and will be random.
	// Todo: Ask user if the item should be added or not.

	mBindings[item->getUUID()] = new exoBindedInventory(item->getUUID(), label, key, mask, true);
	
	gInventory.updateItem((LLViewerInventoryItem*)item);
	gInventory.notifyObservers();

	return;
}

BOOL exoInventoryHotkeys::triggerBinding(KEY key, MASK mask)
{
	std::vector<exoBindedInventory*> matching;
	item_map_t::iterator it;

	for (it = mBindings.begin(); it != mBindings.end(); ++it)
	{
		exoBindedInventory* item = (*it).second;
		if (item && item->mKey == key && item->mMask == mask) matching.push_back(item);
	}

	if (matching.size() > 0)
	{
		U32 random = ll_rand(matching.size());
		exoBindedInventory* binding = matching[random];
		LLViewerInventoryItem* item = gInventory.getItem(binding->mItem);
		toggleInventory(item, binding->mLabel, binding->mReplace);

		return TRUE;
	}

	return FALSE;
}

BOOL exoInventoryHotkeys::toggleInventory(LLViewerInventoryItem* item, std::string label, bool replace)
{
	if (item) switch (item->getType())
	{
		case LLAssetType::AT_OBJECT:
		{
			if (isAgentAvatarValid() && gAgentAvatarp->isWearingAttachment(item->getLinkedUUID()))
			{
				LLVOAvatarSelf::detachAttachmentIntoInventory(item->getLinkedUUID());
				systemTip("\"" + label + "\" has been requested to be detached.");
			}
			else
			{
				rez_attachment(item, NULL, replace);
				systemTip("\"" + label + "\" has been requested to be attached.");

				return TRUE;
			}
			break;
		}
		case LLAssetType::AT_BODYPART:
		case LLAssetType::AT_CLOTHING:
		{
			if (gAgentWearables.isWearingItem(item->getLinkedUUID()))
			{
				LLAppearanceMgr::instance().removeItemFromAvatar(item->getLinkedUUID());
				systemTip("\"" + label + "\" has been requested to be removed.");
			}
			else
			{
				LLAppearanceMgr::instance().wearItemOnAvatar(item->getLinkedUUID(), replace, true);
				systemTip("\"" + label + "\" has been requested to be worn.");

				return TRUE;
			}
			break;
		}
		/*case LLAssetType::AT_GESTURE:
		{
			if (LLGestureMgr::instance().isGestureActive(item->getLinkedUUID()))
			{
				LLGestureMgr::instance().deactivateGesture(item->getLinkedUUID());
				systemTip("\"" + label + "\" has been deactivated.");
			}
			else
			{
				LLGestureMgr::instance().activateGesture(item->getLinkedUUID());
				systemTip("\"" + label + "\" has been activated.");

				return TRUE;
			}
			break;
		}*/
		default:
		{
			systemTip("\"" + label + "\" is of wrong type.");
			
			break;
		}
	}

	return FALSE;
}

void exoInventoryHotkeys::systemTip(std::string data)
{
	if (data.empty()) return;

	LLSD args;
	args["MESSAGE"] = data;
	LLNotificationsUtil::add("SystemMessageTip", args);
}
