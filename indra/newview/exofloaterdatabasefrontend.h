/** 
 * @file exoFloaterDatabaseFrontend.h
 * @brief exoFloaterDatabaseFrontend class definition
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_FLOATER_DATABASE_FRONTEND
#define EXO_FLOATER_DATABASE_FRONTEND

#include "llfloater.h"

class LLProgressBar;
class LLAvatarName;

class exoFloaterDatabaseFrontend :
	public LLFloater
{
	friend class LLFloaterReg;

private:
	exoFloaterDatabaseFrontend(const LLSD& key);
	~exoFloaterDatabaseFrontend();

	BOOL postBuild();

	LLProgressBar* mProgressBar;
	LLUUID mTargetKey;

public:
	void recieveData(LLSD data, bool good);
};

class exoFloaterDatabaseFrontendAdjust :
	public LLFloater
{
	friend class LLFloaterReg;

private:
	exoFloaterDatabaseFrontendAdjust(const LLSD& key);
	~exoFloaterDatabaseFrontendAdjust();

	BOOL postBuild();

	LLUUID mTargetKey;
	std::string mUsername;
	std::string mPassword;

public:
	void setData(std::string name, U32 rank, U32 balance, std::string division);
	void recieveData(LLSD data, bool good);
	void setInputEnabled(bool enabled);
	
public:
	static void onConfirmDelete(const LLSD& notification, const LLSD& response);

public:
	void onClickClose();
	void onClickSubmit();
	void onClickDelete();
};

class exoFloaterDatabaseFrontendInvite :
	public LLFloater
{
	friend class LLFloaterReg;

private:
	exoFloaterDatabaseFrontendInvite(const LLSD& key);
	~exoFloaterDatabaseFrontendInvite();

	BOOL postBuild();

	LLUUID mTargetKey;
	std::string mUsername;
	std::string mPassword;
	
public:
	void onOpen(const LLSD& key);
	void recieveData(LLSD data, bool good);
	void setInputEnabled(bool enabled);

public:
	static void onNameLoaded(const LLUUID& id, const LLAvatarName av_name);

public:
	void onClickClose();
	void onClickSubmit();
	void onClickInvite();
};

#endif // EXO_FLOATER_DATABASE_FRONTEND
