/**
 * @file exonotificationmanager.h
 * @brief Manages display of platform-specific toast systems.
 *
 * $LicenseInfo:firstyear=2012&license=viewerlgpl$
 * Copyright (C) 2012 Katharine Berry
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXONOTIFICATIONMANAGER_H
#define EXONOTIFICATIONMANAGER_H
#include "exonotifier.h"
#include "lleventtimer.h"
#include <map>

class LLNotficationPtr;

struct exoNotification
{
	std::string name;
	std::string title;
	std::string body;
	bool useDefaultTextForTitle;
	bool useDefaultTextForBody;
};

const U64 EXO_NOTIFICATION_THROTTLE_TIME = 1000000; // Maximum spam rate (in microseconds).
const F32 EXO_NOTIFICATION_THROTTLE_CLEANUP_PERIOD = 300; // How often we clean up the list (in seconds).
const int EXO_NOTIFICATION_MAX_BODY_LENGTH = 255; // Arbitrary.
const std::string EXO_NOTIFICATION_IM_MESSAGE_TYPE = "Instant Message received";

class exoNotificationManager : public LLEventTimer, public LLSingleton<exoNotificationManager>
{
public:
	exoNotificationManager();
	~exoNotificationManager();
	
	void initialize();

	void notify(const std::string& notification_title, const std::string& notification_message, const std::string& notification_type);
	BOOL tick();

	void onAppFocusGained();

private:
	exoNotifier *mNotifier;
	std::map<std::string, exoNotification> mNotifications;
	std::map<std::string, U64> mTitleTimers;
	
	void loadConfig();
	bool onLLNotification(const LLSD& notice);
	bool filterOldNotifications(LLNotificationPtr pNotification);
	void onInstantMessage(const LLSD& im);
	static inline bool shouldNotify();
};

#endif // EXONOTIFICATIONMANAGER_H