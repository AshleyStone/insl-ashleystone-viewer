/***********************************
 * exolineartoneF.glsl
 * Provides linear tone mapping functionality.
 * Copyright Geenz Spad, 2012
 ***********************************/
#extension GL_ARB_texture_rectangle : enable

#ifdef DEFINE_GL_FRAGCOLOR
out vec4 gl_FragColor;
#endif

uniform sampler2DRect exo_screen;
uniform float exo_exposure;
VARYING vec2 vary_fragcoord;

void main ()
{
	vec4 diff = texture2DRect(exo_screen, vary_fragcoord.xy);
	diff.rgb *= exo_exposure;  // Hardcoded Exposure Adjustment
	diff.rgb = diff.rgb / (1 + diff.rgb);
	//diff.rgb = pow(diff.rgb, vec3(1.0/2.2));
	gl_FragColor = diff;
}