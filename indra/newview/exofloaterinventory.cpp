/** 
 * @file exofloaterinventory.cpp
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "exofloaterinventory.h"

#include "llagent.h"
#include "llscrolllistctrl.h"
#include "llinventoryfunctions.h"
#include "llinventorymodel.h"
#include "llinventoryobserver.h"
#include "llinventorypanel.h"
#include "llinventoryfilter.h"
#include "llinventoryicon.h"
#include "llviewerinventory.h"
#include "llfolderview.h"
#include "llfoldervieweventlistener.h"

// exo_floater_inventory.xml

// Todo: Replace the scroll list with a extended inventory panel, simular to "places_inventory_panel".

exoFloaterInventory::exoFloaterInventory(const LLSD& key) :
	LLFloater(key)
{
}

exoFloaterInventory::~exoFloaterInventory()
{
}

BOOL exoFloaterInventory::postBuild()
{
	folders = getChild<LLInventoryPanel>("folder_list");

	folders->setShowFolderState(LLInventoryFilter::SHOW_ALL_FOLDERS);
	folders->setFilterTypes(0);

	folders->setSelectCallback(boost::bind(&exoFloaterInventory::onSelectionChange, this, _1, _2));

	contents = getChild<LLScrollListCtrl>("content_list");

	return TRUE;
}

void exoFloaterInventory::onSelectionChange(const std::deque<LLFolderViewItem*>& selection, BOOL user_action)
{
	if (selection.empty()) return;

	contents->clearRows();

	LLUUID id = (*selection.begin())->getListener()->getUUID();
	LLViewerInventoryCategory* catagory = gInventory.getCategory(id);

	if (catagory)
	{
		LLInventoryModel::item_array_t* items;
		LLInventoryModel::cat_array_t* catagories;

		gInventory.getDirectDescendentsOf(id, catagories, items);

		U32 count = catagories->count(); 

		for (U32 index = 0; index < count; ++index)
		{
			LLViewerInventoryCategory* category = catagories->get(index);

			if (category)
			{
				LLSD row;

				row["columns"][0]["type"] = "icon";
				row["columns"][0]["name"] = "icon";
				row["columns"][0]["value"] = "Inv_FolderClosed";

				row["columns"][1]["type"] = "text";
				row["columns"][1]["name"] = "name";
				row["columns"][1]["value"] = category->getName();
			
				U32 descendants = category->getDescendentCount();

				row["columns"][2]["type"] = "text";
				row["columns"][2]["name"] = "descripton";

				if (descendants == 1) row["columns"][2]["value"] = "1 item";
				else row["columns"][2]["value"] = llformat("%d items", category->getDescendentCount());

				contents->addElement(row);
			}
		}

		LLUUID id = gAgent.getID();

		count = items->count(); 

		for (U32 index = 0; index < count; ++index)
		{
			LLViewerInventoryItem* item = items->get(index);

			if (item && item->isFinished())
			{
				LLSD row;

				row["columns"][0]["type"] = "icon";
				row["columns"][0]["name"] = "icon";
				row["columns"][0]["value"] = LLInventoryIcon::getIconName(item->getType());

				row["columns"][1]["type"] = "text";
				row["columns"][1]["name"] = "name";
				row["columns"][1]["value"] = item->getName();
			
				row["columns"][2]["type"] = "text";
				row["columns"][2]["name"] = "descripton";
				row["columns"][2]["value"] = item->getDescription();
				
				time_t acquired = item->getCreationDate();
				
				row["columns"][3]["type"] = "text";
				row["columns"][3]["name"] = "acquired";

				if (0 == acquired)
				{
					row["columns"][3]["value"] = getString("unknown");
				}
				else
				{
					std::string time = getString("date");
					LLSD substitution;
					substitution["datetime"] = (S32)acquired;
					LLStringUtil::format(time, substitution);

					row["columns"][3]["value"] = time;
				}

				const LLPermissions perms = item->getPermissions();
				
				row["columns"][4]["type"] = "text";
				row["columns"][4]["name"] = "modify";
				row["columns"][4]["value"] = perms.allowModifyBy(id) ? "X" : " ";
				
				row["columns"][5]["type"] = "text";
				row["columns"][5]["name"] = "copy";
				row["columns"][5]["value"] = perms.allowCopyBy(id) ? "X" : " ";
				
				row["columns"][6]["type"] = "text";
				row["columns"][6]["name"] = "transfer";
				row["columns"][6]["value"] = perms.allowCopyBy(id) ? "X" : " ";

				contents->addElement(row);
			}
		}
	}
}
