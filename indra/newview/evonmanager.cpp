/** 
 * @file evonmanager.cpp
 * @brief System used for downloading client detection database and other bits.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "evonmanager.h"

#include "llsdserialize.h"

#define EVON_LABEL "EVON"
#define EVON_VERSION "1.0"
#define EVON_VERSION_NUM 1.0

S32 evonManager::toEVON(const LLSD& sd, std::ostream& str, std::string type, std::string version)
{
	str << "# " << EVON_LABEL << " " << EVON_VERSION << " " << type << " " << version << "\n";

	return LLSDSerialize::toJSON(sd, str);
}

S32 evonManager::toPrettyEVON(const LLSD& sd, std::ostream& str, std::string type, std::string version)
{
	str << "# " << EVON_LABEL << " " << EVON_VERSION << " " << type << " " << version << "\n";

	return LLSDSerialize::toPrettyJSON(sd, str);
}

S32 evonManager::fromEVON(LLSD& sd, std::istream& str, std::string type, double version)
{
	std::string versionData;
	std::getline(str, versionData);

	if (!versionData.empty())
	{
		std::vector<std::string> versionTokens;
		LLStringUtil::getTokens(versionData, versionTokens, " ");

		if (versionTokens[0] == "#")
		{
			if (versionTokens[1] == EVON_LABEL)
			{
				if (atof(versionTokens[2].c_str()) <= EVON_VERSION_NUM)
				{
					if (versionTokens[3] == type)
					{
						if (atof(versionTokens[4].c_str()) <= version)
						{
							return LLSDSerialize::fromJSON(sd, str);
						}
						else llinfos << "Import version is \"" << versionTokens[4] << "\" not \"" << version << "\", failed to import." << llendl;
					}
					else llinfos << "Import type is \"" << versionTokens[3] << "\" not \"" << type << "\", failed to import." << llendl;
				}
				else llinfos << "File version is \"" << versionTokens[2] << "\" not \"" << EVON_VERSION << "\", failed to import." << llendl;
			}
			else llinfos << "File type is \"" << versionTokens[1] << "\" not \"" << EVON_LABEL << "\", failed to import." << llendl;
		}
		else llinfos << "File type is incorrect, did not detect \"#\" on first line, failed to import." << llendl;
	}
	else llinfos << "Unable to get first line, failed to import." << llendl;

	return 0;
}
