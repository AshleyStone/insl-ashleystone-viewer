/** 
 * @file exofloateradvancedgraphics.cpp
 * @brief The advanced graphics window for adjusting advanced shaders.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "exofloateradvancedgraphics.h"

#include "evonmanager.h"
#include "message.h"

#include "llassetuploadresponders.h"
#include "llagent.h"
#include "llcombobox.h"
#include "lldiriterator.h"
#include "llinventoryfunctions.h"
#include "llinventorymodel.h"
#include "llfile.h"
#include "llfilepicker.h"
#include "llfloaterreg.h"
#include "llnotecard.h"
#include "llnotificationsutil.h"
#include "llslider.h"
#include "llspinctrl.h"
#include "llvfile.h"
#include "llvfs.h"
#include "llviewercontrol.h"
#include "llviewerinventory.h"
#include "llviewerregion.h"
#include "llextendedstatus.h"

#define EVON_TYPE "AG"
#define EVON_EXTENTION ".ag.evon"
#define EVON_VERSION "1.1"
#define EVON_VERSION_NUM 1.1

#define PRESET_FOLDER "#Advanced Graphics Presets"

// exodus_floater_advanced_graphics.xml

// Todo: Escape LL_PATH_USER_SETTINGS preset filenames?

exoFloaterAdvancedGraphics::exoFloaterAdvancedGraphics(const LLSD& key) :
	LLFloater(key)
{
	// Commit callback for resetting settings.
	mCommitCallbackRegistrar.add("Exodus.ResetSetting", boost::bind(&exoFloaterAdvancedGraphics::resetSetting, this, _2));
	mCommitCallbackRegistrar.add("Exodus.ResetSpecial", boost::bind(&exoFloaterAdvancedGraphics::resetSpecial, this, _2));
}

BOOL exoFloaterAdvancedGraphics::postBuild()
{
	// Preset Manager
	{
		LLComboBox* presetCombo = getChild<LLComboBox>("PresetsCombo");
		if (presetCombo)
		{
			presetCombo->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onSelectPreset, this, _1));

			loadPresets(gDirUtilp->getExpandedFilename(LL_PATH_APP_SETTINGS, "evon", "advanced_graphics", ""), presetCombo);
			presetCombo->addSeparator();
			loadPresets(gDirUtilp->getExpandedFilename(LL_PATH_USER_SETTINGS, "evon", "advanced_graphics", ""), presetCombo, true);

			if (!presetCombo->getItemCount()) presetCombo->add("Default");

			if (!presetCombo->selectByValue(LLSD(gSavedSettings.getString("ExodusAdvancedGraphicsPreset"))))
			{
				llwarns << "Unable to find preset \"" << gSavedSettings.getString("ExodusAdvancedGraphicsPreset") << "\"." << llendl;
				presetCombo->selectByValue(LLSD("Default"));
			}
		}

		LLButton* saveButton = getChild<LLButton>("Save");
		if (saveButton) saveButton->setClickedCallback(boost::bind(&exoFloaterAdvancedGraphics::savePreset, this));

		LLButton* nextPresetButton = getChild<LLButton>("NextPreset");
		if (nextPresetButton) nextPresetButton->setClickedCallback(boost::bind(&exoFloaterAdvancedGraphics::nextPreset, this));

		LLButton* prevPresetButton = getChild<LLButton>("PrevPreset");
		if (prevPresetButton) prevPresetButton->setClickedCallback(boost::bind(&exoFloaterAdvancedGraphics::previousPreset, this));

		LLButton* addPresetButton = getChild<LLButton>("Add");
		if (addPresetButton) addPresetButton->setClickedCallback(boost::bind(&exoFloaterAdvancedGraphics::addPreset, this));

		LLButton* removeButton = getChild<LLButton>("Remove");
		if (removeButton) removeButton->setClickedCallback(boost::bind(&exoFloaterAdvancedGraphics::removePreset, this));
	}
	// Import/Export
	{
		LLButton* importButton = getChild<LLButton>("Import");
		if (importButton) importButton->setClickedCallback(boost::bind(&exoFloaterAdvancedGraphics::importFile, this));

		LLButton* exportButton = getChild<LLButton>("Export");
		if (exportButton) exportButton->setClickedCallback(boost::bind(&exoFloaterAdvancedGraphics::exportData, this));
	}
	// Gamma UI Callbacks
	{
		getChild<LLSpinCtrl>("GammaRedSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustGammaRedSpinner, this));
		getChild<LLSlider>("GammaRed")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustGammaRed, this));
		getChild<LLSpinCtrl>("GammaGreenSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustGammaGreenSpinner, this));
		getChild<LLSlider>("GammaGreen")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustGammaGreen, this));
		getChild<LLSpinCtrl>("GammaBlueSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustGammaBlueSpinner, this));
		getChild<LLSlider>("GammaBlue")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustGammaBlue, this));
	}
	// Exposure UI Callbacks
	{
		getChild<LLSpinCtrl>("ExposureRedSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustExposureRedSpinner, this));
		getChild<LLSlider>("ExposureRed")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustExposureRed, this));
		getChild<LLSpinCtrl>("ExposureGreenSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustExposureGreenSpinner, this));
		getChild<LLSlider>("ExposureGreen")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustExposureGreen, this));
		getChild<LLSpinCtrl>("ExposureBlueSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustExposureBlueSpinner, this));
		getChild<LLSlider>("ExposureBlue")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustExposureBlue, this));
	}
	// Offset UI Callbacks
	{
		getChild<LLSpinCtrl>("OffsetRedSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustOffsetRedSpinner, this));
		getChild<LLSlider>("OffsetRed")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustOffsetRed, this));
		getChild<LLSpinCtrl>("OffsetGreenSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustOffsetGreenSpinner, this));
		getChild<LLSlider>("OffsetGreen")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustOffsetGreen, this));
		getChild<LLSpinCtrl>("OffsetBlueSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustOffsetBlueSpinner, this));
		getChild<LLSlider>("OffsetBlue")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustOffsetBlue, this));
	}
	// Vignette UI Callbacks
	{
		getChild<LLSpinCtrl>("VignetteSpinnerX")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustVignetteSpinnerX, this));
		getChild<LLSlider>("VignetteSliderX")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustVignetteX, this));
		getChild<LLSpinCtrl>("VignetteSpinnerY")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustVignetteSpinnerY, this));
		getChild<LLSlider>("VignetteSliderY")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustVignetteY, this));
		getChild<LLSpinCtrl>("VignetteSpinnerZ")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustVignetteSpinnerZ, this));
		getChild<LLSlider>("VignetteSliderZ")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustVignetteZ, this));
	}
	// Advanced Tone Mapping Callbacks
	{
		getChild<LLSpinCtrl>("ShoulderStrengthSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneShoulderStrSpinner, this));
		getChild<LLSlider>("ShoulderStrengthSlider")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneShoulderStr, this));
		getChild<LLSpinCtrl>("LinearStrengthSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneLinearStrSpinner, this));
		getChild<LLSlider>("LinearStrengthSlider")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneLinearStr, this));
		getChild<LLSpinCtrl>("LinearAngleSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneLinearAngleSpinner, this));
		getChild<LLSlider>("LinearAngleSlider")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneLinearAngle, this));
		
		getChild<LLSpinCtrl>("ToeStrengthSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneToeStrSpinner, this));
		getChild<LLSlider>("ToeStrengthSlider")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneToeStr, this));
		getChild<LLSpinCtrl>("ToeNumeratorSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneToeNumeratorSpinner, this));
		getChild<LLSlider>("ToeNumeratorSlider")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneToeNumerator, this));
		getChild<LLSpinCtrl>("ToeDenominatorSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneToeDenominatorSpinner, this));
		getChild<LLSlider>("ToeDenominatorSlider")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneToeDenominator, this));
		
		getChild<LLSpinCtrl>("LinearWhitePointSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneLinearWhitePointSpinner, this));
		getChild<LLSlider>("LinearWhitePointSlider")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneLinearWhitePoint, this));
		getChild<LLSpinCtrl>("ExposureBiasSpinner")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneExposureBiasSpinner, this));
		getChild<LLSlider>("ExposureBiasSlider")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustAdvToneExposureBias, this));

		gSavedSettings.getControl("ExodusRenderToneMappingTech")->getSignal()->connect(boost::bind(&exoFloaterAdvancedGraphics::updateToneMapping, this));
		gSavedSettings.getControl("ExodusRenderToneMapping")->getSignal()->connect(boost::bind(&exoFloaterAdvancedGraphics::updateToneMapping, this));
	}
	
	// Shadows
	{
		// Update text visibility, hide if nessecery.
		updateNotes(LLSD());

		// Update UI setting, for hacky slider/spinner usage on Vector3.
		gSavedSettings.setF32("ExodusRenderSSAOEffect", gSavedSettings.getVector3("RenderSSAOEffect").mV[0]);

		// Connect various settings to callbacks.
		gSavedSettings.getControl("RenderDeferred")->getSignal()->connect(boost::bind(&exoFloaterAdvancedGraphics::updateNotes, this, _2));
		gSavedSettings.getControl("RenderDeferredSSAO")->getSignal()->connect(boost::bind(&exoFloaterAdvancedGraphics::updateNotes, this, _2));
		gSavedSettings.getControl("ExodusRenderSSAOEffect")->getSignal()->connect(boost::bind(&exoFloaterAdvancedGraphics::updateAmbientOcclusion, this, _2));
		gSavedSettings.getControl("RenderShadowDetail")->getSignal()->connect(boost::bind(&exoFloaterAdvancedGraphics::updateNotes, this, _2));
		
		// Callbacks for the XYZ settings for "RenderShadowSplitExponent".
		getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerX")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustRSSE, this));
		getChild<LLSlider>("RenderShadowSplitExponentSliderX")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustRSSE2, this));
		getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerY")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustRSSE, this));
		getChild<LLSlider>("RenderShadowSplitExponentSliderY")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustRSSE2, this));
		getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerZ")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustRSSE, this));
		getChild<LLSlider>("RenderShadowSplitExponentSliderZ")->setCommitCallback(boost::bind(&exoFloaterAdvancedGraphics::onAdjustRSSE2, this));
	}
	updateUI();
	return TRUE;
}

void exoFloaterAdvancedGraphics::updateUI()
{
	// Gamma UI
	{
		LLVector3 gamma = gSavedSettings.getVector3("ExodusRenderGamma");
		
		getChild<LLSpinCtrl>("GammaRedSpinner")->setValue(gamma.mV[VX]);
		getChild<LLSlider>("GammaRed")->setValue(gamma.mV[VX]);
		getChild<LLSpinCtrl>("GammaGreenSpinner")->setValue(gamma.mV[VY]);
		getChild<LLSlider>("GammaGreen")->setValue(gamma.mV[VY]);
		getChild<LLSpinCtrl>("GammaBlueSpinner")->setValue(gamma.mV[VZ]);
		getChild<LLSlider>("GammaBlue")->setValue(gamma.mV[VZ]);
	}
	// Exposure UI
	{
		LLVector3 exposure = gSavedSettings.getVector3("ExodusRenderExposure");
		
		getChild<LLSpinCtrl>("ExposureRedSpinner")->setValue(exposure.mV[VX]);
		getChild<LLSlider>("ExposureRed")->setValue(exposure.mV[VX]);
		getChild<LLSpinCtrl>("ExposureGreenSpinner")->setValue(exposure.mV[VY]);
		getChild<LLSlider>("ExposureGreen")->setValue(exposure.mV[VY]);
		getChild<LLSpinCtrl>("ExposureBlueSpinner")->setValue(exposure.mV[VZ]);
		getChild<LLSlider>("ExposureBlue")->setValue(exposure.mV[VZ]);
	}
	// Offset UI
	{
		LLVector3 offset = gSavedSettings.getVector3("ExodusRenderOffset");
		
		getChild<LLSpinCtrl>("OffsetRedSpinner")->setValue(offset.mV[VX]);
		getChild<LLSlider>("OffsetRed")->setValue(offset.mV[VX]);
		getChild<LLSpinCtrl>("OffsetGreenSpinner")->setValue(offset.mV[VY]);
		getChild<LLSlider>("OffsetGreen")->setValue(offset.mV[VY]);
		getChild<LLSpinCtrl>("OffsetBlueSpinner")->setValue(offset.mV[VZ]);
		getChild<LLSlider>("OffsetBlue")->setValue(offset.mV[VZ]);
	}
	// Vignette UI
	{
		LLVector3 vignette = gSavedSettings.getVector3("ExodusRenderVignette");
		
		getChild<LLSpinCtrl>("VignetteSpinnerX")->setValue(vignette.mV[VX]);
		getChild<LLSlider>("VignetteSliderX")->setValue(vignette.mV[VX]);
		getChild<LLSpinCtrl>("VignetteSpinnerY")->setValue(vignette.mV[VY]);
		getChild<LLSlider>("VignetteSliderY")->setValue(vignette.mV[VY]);
		getChild<LLSpinCtrl>("VignetteSpinnerZ")->setValue(vignette.mV[VZ]);
		getChild<LLSlider>("VignetteSliderZ")->setValue(vignette.mV[VZ]);
	}
	// Advanced Tone Mapping
	{
		LLVector3 tone = gSavedSettings.getVector3("ExodusRenderToneAdvOptA");
		getChild<LLSpinCtrl>("ShoulderStrengthSpinner")->setValue(tone.mV[VX]);
		getChild<LLSlider>("ShoulderStrengthSlider")->setValue(tone.mV[VX]);
		getChild<LLSpinCtrl>("LinearStrengthSpinner")->setValue(tone.mV[VY]);
		getChild<LLSlider>("LinearStrengthSlider")->setValue(tone.mV[VY]);
		getChild<LLSpinCtrl>("LinearAngleSpinner")->setValue(tone.mV[VZ]);
		getChild<LLSlider>("LinearAngleSlider")->setValue(tone.mV[VZ]);
		
		tone = gSavedSettings.getVector3("ExodusRenderToneAdvOptB");
		getChild<LLSpinCtrl>("ToeStrengthSpinner")->setValue(tone.mV[VX]);
		getChild<LLSlider>("ToeStrengthSlider")->setValue(tone.mV[VX]);
		getChild<LLSpinCtrl>("ToeNumeratorSpinner")->setValue(tone.mV[VY]);
		getChild<LLSlider>("ToeNumeratorSlider")->setValue(tone.mV[VY]);
		getChild<LLSpinCtrl>("ToeDenominatorSpinner")->setValue(tone.mV[VZ]);
		getChild<LLSlider>("ToeDenominatorSlider")->setValue(tone.mV[VZ]);
		
		tone = gSavedSettings.getVector3("ExodusRenderToneAdvOptC");
		getChild<LLSpinCtrl>("LinearWhitePointSpinner")->setValue(tone.mV[VX]);
		getChild<LLSlider>("LinearWhitePointSlider")->setValue(tone.mV[VX]);
		getChild<LLSpinCtrl>("ExposureBiasSpinner")->setValue(tone.mV[VY]);
		getChild<LLSlider>("ExposureBiasSlider")->setValue(tone.mV[VY]);
		
		updateToneMapping();
	}
	
	// Shadows
	{
		// Update text visibility, hide if neccessary.
		updateNotes(LLSD());
		
		// Update UI setting, for hacky slider/spinner usage on Vector3.
		gSavedSettings.setF32("ExodusRenderSSAOEffect", gSavedSettings.getVector3("RenderSSAOEffect").mV[0]);
		
		LLVector3 rsse = gSavedSettings.getVector3("RenderShadowSplitExponent");
		getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerX")->setValue(rsse.mV[VX]);
		getChild<LLSlider>("RenderShadowSplitExponentSliderX")->setValue(rsse.mV[VX]);
		getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerY")->setValue(rsse.mV[VY]);
		getChild<LLSlider>("RenderShadowSplitExponentSliderY")->setValue(rsse.mV[VY]);
		getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerZ")->setValue(rsse.mV[VZ]);
		getChild<LLSlider>("RenderShadowSplitExponentSliderZ")->setValue(rsse.mV[VZ]);
	}
}

/* ---------------------------------------------------------------------------- */
/* SHADOW TAB																	*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::onAdjustRSSE()
{
	LLVector3 rsse;
	
	rsse.mV[VX] = getChild<LLSlider>("RenderShadowSplitExponentSpinnerX")->getValueF32();
	rsse.mV[VY] = getChild<LLSlider>("RenderShadowSplitExponentSpinnerY")->getValueF32();
	rsse.mV[VZ] = getChild<LLSlider>("RenderShadowSplitExponentSpinnerZ")->getValueF32();

	getChild<LLSlider>("RenderShadowSplitExponentSliderX")->setValue(rsse.mV[VX]);
	getChild<LLSlider>("RenderShadowSplitExponentSliderY")->setValue(rsse.mV[VY]);
	getChild<LLSlider>("RenderShadowSplitExponentSliderZ")->setValue(rsse.mV[VZ]);

	gSavedSettings.setVector3("RenderShadowSplitExponent", rsse);
}

void exoFloaterAdvancedGraphics::onAdjustRSSE2()
{
	LLVector3 rsse;

	rsse.mV[VX] = getChild<LLSlider>("RenderShadowSplitExponentSliderX")->getValueF32();
	rsse.mV[VY] = getChild<LLSlider>("RenderShadowSplitExponentSliderY")->getValueF32();
	rsse.mV[VZ] = getChild<LLSlider>("RenderShadowSplitExponentSliderZ")->getValueF32();

	getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerX")->setValue(rsse.mV[VX]);
	getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerY")->setValue(rsse.mV[VY]);
	getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerZ")->setValue(rsse.mV[VZ]);

	gSavedSettings.setVector3("RenderShadowSplitExponent", rsse);
}

bool exoFloaterAdvancedGraphics::updateNotes(const LLSD& newvalue)
{
	getChildView("Deferred Note 1")->setVisible(!gSavedSettings.getBOOL("RenderDeferred"));
	getChildView("Deferred Note 2")->setVisible(!gSavedSettings.getBOOL("RenderDeferred"));

	// Disable/enable everything in the ambient occlusion panel.
	bool occluding = gSavedSettings.getBOOL("RenderDeferredSSAO");
	getChildView("occlusion_warning")->setVisible(!occluding);

	LLView* occlusion_panel = getChildView("ambient_occlusion_Panel");
	std::for_each(occlusion_panel->beginChild(), occlusion_panel->endChild(), boost::bind(&LLView::setEnabled, _1, occluding));
	
	// Same for the shadow panel.
	bool shadows = gSavedSettings.getS32("RenderShadowDetail");
	LLView* shadow_panel = getChildView("shadow_Customization_Panel");
	std::for_each(shadow_panel->beginChild(), shadow_panel->endChild(), boost::bind(&LLView::setEnabled, _1, shadows));
	
	return true;
}

bool exoFloaterAdvancedGraphics::updateAmbientOcclusion(const LLSD& newvalue)
{
	LLVector3 ssao = gSavedSettings.getVector3("RenderSSAOEffect");
	ssao.mV[0] = newvalue.asReal();

	gSavedSettings.setVector3("RenderSSAOEffect", ssao);

	return true;
}

bool exoFloaterAdvancedGraphics::updateToneMapping()
{
	bool enable = (gSavedSettings.getS32("ExodusRenderToneMappingTech") == 3 &&
				   gSavedSettings.getBOOL("ExodusRenderToneMapping"));
	LLView* panel = getChildView("advanced_tonemapping_panel");
	std::for_each(panel->beginChild(), panel->endChild(), boost::bind(&LLView::setEnabled, _1, enable));
	return true;
}

void exoFloaterAdvancedGraphics::resetSetting(const LLSD& user_data)
{
	std::string setting = user_data.asString();
	if (gSavedSettings.controlExists(setting))
	{
		gSavedSettings.getControl(setting)->resetToDefault(true);
	}
}

void exoFloaterAdvancedGraphics::resetSpecial(const LLSD& user_data)
{
	std::string setting = user_data.asString();

	if (setting == "RenderShadowSplitExponentX")
	{
		LLVector3 rsseO = gSavedSettings.getVector3("RenderShadowSplitExponent");
		LLVector3 rsseD = LLVector3(gSavedSettings.getControl("RenderShadowSplitExponent")->getDefault());
		LLVector3 rsseN = LLVector3(rsseD.mV[VX], rsseO.mV[VY], rsseO.mV[VZ]);
		
		gSavedSettings.setVector3("RenderShadowSplitExponent", rsseN);

		getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerX")->setValue(rsseN.mV[VX]);
		getChild<LLSlider>("RenderShadowSplitExponentSliderX")->setValue(rsseN.mV[VX]);
	}
	else if(setting == "RenderShadowSplitExponentY")
	{
		LLVector3 rsseO = gSavedSettings.getVector3("RenderShadowSplitExponent");
		LLVector3 rsseD = LLVector3(gSavedSettings.getControl("RenderShadowSplitExponent")->getDefault());
		LLVector3 rsseN = LLVector3(rsseO.mV[VX], rsseD.mV[VY], rsseO.mV[VZ]);
		
		gSavedSettings.setVector3("RenderShadowSplitExponent", rsseN);

		getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerY")->setValue(rsseN.mV[VY]);
		getChild<LLSlider>("RenderShadowSplitExponentSliderY")->setValue(rsseN.mV[VY]);
	}
	else if(setting == "RenderShadowSplitExponentZ")
	{
		LLVector3 rsseO = gSavedSettings.getVector3("RenderShadowSplitExponent");
		LLVector3 rsseD = LLVector3(gSavedSettings.getControl("RenderShadowSplitExponent")->getDefault());
		LLVector3 rsseN = LLVector3(rsseO.mV[VX], rsseO.mV[VY], rsseD.mV[VZ]);
		
		gSavedSettings.setVector3("RenderShadowSplitExponent", rsseN);

		getChild<LLSpinCtrl>("RenderShadowSplitExponentSpinnerZ")->setValue(rsseN.mV[VZ]);
		getChild<LLSlider>("RenderShadowSplitExponentSliderZ")->setValue(rsseN.mV[VZ]);
	}
}

/* ---------------------------------------------------------------------------- */
/* SAVING PRESETS																*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::savePreset()
{
	LLComboBox* comboBox = getChild<LLComboBox>("PresetsCombo");
	if (comboBox)
	{
		std::string label = comboBox->getSelectedItemLabel();
		if (label.empty() ||
			label[label.length() - 1] != '*')
		{
			return;
		}

		label = label.substr(0, label.length() - 1);

		LLSD args, payload;
		args["NAME"] = label;
		payload["preset_name"] = label;

		LLNotificationsUtil::add("ExodusAdvancedGraphicsSave", args, payload, boost::bind(savePresetCallback, _1, _2));
	}
}

void exoFloaterAdvancedGraphics::savePresetCallback(const LLSD& notification, const LLSD& response)
{
	if (LLNotificationsUtil::getSelectedOption(notification, response) == 0)
	{
		std::string label = notification["payload"]["preset_name"].asString();
		if (label.empty()) return;

		exoFloaterAdvancedGraphics* self =
			LLFloaterReg::getTypedInstance<exoFloaterAdvancedGraphics>("exo_advanced_graphics");
		if (self)
		{
			self->savePresetFinal(label);
		}
	}
}

void exoFloaterAdvancedGraphics::savePresetFinal(std::string label)
{
	LLSD data = generateData();
	llofstream evon_file(gDirUtilp->getExpandedFilename(LL_PATH_USER_SETTINGS, "evon", "advanced_graphics", label + EVON_EXTENTION));
	if (evon_file.is_open())
	{
		evonManager::toPrettyEVON(data, evon_file, EVON_TYPE, EVON_VERSION);
		evon_file.close();
	}
	else
	{
		llwarns << "Unable to save preset \"" << label << "\"." << llendl;
	}
}

/* ---------------------------------------------------------------------------- */
/* ADDING/REMOVING PRESETS														*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::addPreset()
{
	LLComboBox* comboBox = getChild<LLComboBox>("PresetsCombo");
	if (comboBox)
	{
		LLNotificationsUtil::add("ExodusAdvancedGraphicsEntry", NULL, NULL, boost::bind(addPresetCallback, _1, _2));
	}
}

void exoFloaterAdvancedGraphics::addPresetCallback(const LLSD& notification, const LLSD& response)
{
	if (LLNotificationsUtil::getSelectedOption(notification, response) == 0)
	{
		std::string label = gDirUtilp->getScrubbedFileName(response["preset_name"].asString());
		if (label.empty()) return;
		
		exoFloaterAdvancedGraphics* self =
			LLFloaterReg::getTypedInstance<exoFloaterAdvancedGraphics>("exo_advanced_graphics");
		if (self)
		{
			LLComboBox* presetCombo = self->getChild<LLComboBox>("PresetsCombo");
			if (presetCombo)
			{
				if (presetCombo->itemExists(label))
				{
					LLSD args;
					args["NAME"] = label;
					LLNotificationsUtil::add("ExodusAdvancedGraphicsEntryExists", args, NULL, boost::bind(addPresetCallback, _1, _2));
					return;
				}
				else if (presetCombo->itemExists(label + "*"))
				{
					if (!notification["payload"].has("override") ||
						 notification["payload"]["override"].asString() != label)
					{
						LLSD args, payload;
						args["NAME"] = label;
						payload["override"] = label;
						LLNotificationsUtil::add("ExodusAdvancedGraphicsEntryOverride", args, payload, boost::bind(addPresetCallback, _1, _2));
						return;
					}
				}
			}

			LLSD data = self->generateData();
			llofstream evon_file(gDirUtilp->getExpandedFilename(LL_PATH_USER_SETTINGS, "evon", "advanced_graphics", label + EVON_EXTENTION));
			if (evon_file.is_open())
			{
				evonManager::toPrettyEVON(data, evon_file, EVON_TYPE, EVON_VERSION);
				evon_file.close();
				
				label += "*";
				if (presetCombo->getItemCount() == 1) presetCombo->addSeparator();
				if (!presetCombo->selectByValue(label))
				{
					presetCombo->add(label);
					presetCombo->selectByValue(label);

					gSavedSettings.setString("ExodusAdvancedGraphicsPreset", label);

					self->selectPreset(label);
				}
			}
			else
			{
				llwarns << "Unable to add preset \"" << label << "\"." << llendl;
			}
		}
	}
}

void exoFloaterAdvancedGraphics::removePreset()
{
	LLComboBox* presetCombo = getChild<LLComboBox>("PresetsCombo");
	if (presetCombo)
	{
		std::string victim = presetCombo->getSelectedItemLabel();
		if (victim.empty() ||
			victim[victim.length() - 1] != '*')
		{
			return;
		}
		
		LLSD args, payload;
		args["NAME"] = victim;
		payload["victim"] = victim;

		LLNotificationsUtil::add("ExodusAdvancedGraphicsRemove", args, payload, boost::bind(removePresetCallback, _1, _2));
	}
}

void exoFloaterAdvancedGraphics::removePresetCallback(const LLSD& notification, const LLSD& response)
{
	if (LLNotificationsUtil::getSelectedOption(notification, response) == 0)
	{
		std::string victim = notification["payload"]["victim"].asString();
		if (victim.empty()) return;

		exoFloaterAdvancedGraphics* self =
			LLFloaterReg::getTypedInstance<exoFloaterAdvancedGraphics>("exo_advanced_graphics");
		if (self)
		{
			LLComboBox* presetCombo = self->getChild<LLComboBox>("PresetsCombo");
			if (presetCombo)
			{
				victim = victim.substr(0, victim.length() - 1);
				std::string filename = gDirUtilp->getExpandedFilename(LL_PATH_USER_SETTINGS, "evon", "advanced_graphics", victim + EVON_EXTENTION);
				if (gDirUtilp->fileExists(filename))
				{
					LLFile::remove(victim);
					presetCombo->remove(victim + "*");
					presetCombo->setLabel(LLStringExplicit(""));
					self->getChild<LLButton>("Remove")->setEnabled(false);
				}
				else
				{
					llwarns << "Unable to find file \"" << victim << "." << EVON_EXTENTION << "\"." << llendl;
				}
			}
			else
			{
				llwarns << "Unable to find preset combo." << llendl;
			}
		}
	}
}

/* ---------------------------------------------------------------------------- */
/* CHANGING PRESETS																*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::previousPreset()
{
	LLComboBox* comboBox = getChild<LLComboBox>("PresetsCombo");
	if (comboBox)
	{
		int previousIndex = comboBox->getCurrentIndex() - 1;
		int lastItem = comboBox->getItemCount() - 1;
		if (previousIndex < 0) previousIndex = lastItem;
		else if (previousIndex > lastItem) previousIndex = 0;
		
		if (!comboBox->setCurrentByIndex(previousIndex)) // Because of the separator, bleh.
		{
			previousIndex -= 1;
			comboBox->setCurrentByIndex(previousIndex);
		}

		selectPreset(comboBox->getValue().asString());
	}
}

void exoFloaterAdvancedGraphics::nextPreset()
{
	LLComboBox* comboBox = getChild<LLComboBox>("PresetsCombo");
	if (comboBox)
	{
		int nextIndex = comboBox->getCurrentIndex() + 1;
		int lastItem = comboBox->getItemCount() - 1;
		if (nextIndex < 0) nextIndex = lastItem;
		else if (nextIndex > lastItem) nextIndex = 0;

		if (!comboBox->setCurrentByIndex(nextIndex)) // Because of the separator, bleh.
		{
			nextIndex += 1;
			comboBox->setCurrentByIndex(nextIndex);
		}

		selectPreset(comboBox->getValue().asString());
	}
}

void exoFloaterAdvancedGraphics::onSelectPreset(LLUICtrl* ctrl)
{
	std::string data = ctrl->getValue().asString();
	if (!data.empty()) selectPreset(data);
}

void exoFloaterAdvancedGraphics::selectPreset(std::string requestName)
{
	if (requestName.empty()) return;

	if (requestName.length() > 1 &&
		requestName[requestName.length() - 1] == '*')
	{
		requestName = requestName.substr(0, requestName.length() - 1);
	}

	LLSD data;
	const std::string filename = gDirUtilp->getExpandedFilename(LL_PATH_USER_SETTINGS, "evon", "advanced_graphics", requestName + EVON_EXTENTION);
	llifstream evon_file(filename);
	if (evon_file.is_open() && evonManager::fromEVON(data, evon_file, EVON_TYPE, EVON_VERSION_NUM) > 0) // Custom presets...
	{
		evon_file.close();
		loadPreset(data);

		gSavedSettings.setString("ExodusAdvancedGraphicsPreset", requestName + "*");

		updateButtons(true);
	}
	else // Default presets...
	{
		const std::string filename = gDirUtilp->getExpandedFilename(LL_PATH_APP_SETTINGS, "evon", "advanced_graphics", requestName + EVON_EXTENTION);
		llifstream evon_file(filename);
		if (evon_file.is_open() && evonManager::fromEVON(data, evon_file, EVON_TYPE, EVON_VERSION_NUM) > 0)
		{
			evon_file.close();
			loadPreset(data);

			gSavedSettings.setString("ExodusAdvancedGraphicsPreset", requestName);
		}
		else
		{
			llwarns << "Unable to select preset labeled \"" << requestName << "\"!" << llendl;
		}

		updateButtons(false);
	}
}

void exoFloaterAdvancedGraphics::updateButtons(bool custom)
{
	getChild<LLButton>("Remove")->setEnabled(custom);
	getChild<LLButton>("Save")->setEnabled(custom);
	getChild<LLButton>("Export")->setEnabled(custom);
}

/* ---------------------------------------------------------------------------- */
/* LOAD PRESETS																	*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::loadPresets(std::string dir, LLComboBox* combo, bool user)
{
	std::string exten = "*";
	exten += EVON_EXTENTION;
	LLDirIterator dir_iter(dir, exten);
	while (true)
	{
		std::string file;
		if (!dir_iter.next(file)) break;
		file = gDirUtilp->getBaseFileName(file, true, true);
		if (user) file += "*";
		combo->add(file);
	}
}

void exoFloaterAdvancedGraphics::loadPreset(LLSD data)
{
	if (data.has("Gamma"))
	{
		LLVector3 gammaSetting(data["Gamma"]);
		gSavedSettings.setVector3("ExodusRenderGamma", gammaSetting);
	}

	if (data.has("Exposure"))
	{
		LLVector3 exposureSetting(data["Exposure"]);
		gSavedSettings.setVector3("ExodusRenderExposure", exposureSetting);
	}

	if (data.has("Offset"))
	{
		LLVector3 offsetSetting(data["Offset"]);
		gSavedSettings.setVector3("ExodusRenderOffset", offsetSetting);
	}

	if (data.has("Vignette"))
	{
		LLVector3 vignetteSetting(data["Vignette"]);
		gSavedSettings.setVector3("ExodusRenderVignette", vignetteSetting);
	}
	else
	{
		gSavedSettings.getControl("ExodusRenderVignette")->resetToDefault(true);
	}

	if (data.has("ToneMappingTech"))
	{
		gSavedSettings.setS32("ExodusRenderToneMappingTech", data["ToneMappingTech"].asInteger());
	}
	else
	{
		gSavedSettings.getControl("ExodusRenderToneMappingTech")->resetToDefault(true);
	}
	if (data.has("ToneExposure"))
	{
		gSavedSettings.setF32("ExodusRenderToneExposure", data["ToneExposure"].asReal());
	}
	else
	{
		gSavedSettings.getControl("ExodusRenderToneExposure")->resetToDefault(true);
	}
	if(data.has("ToneAdvOptA"))
	{
		gSavedSettings.setVector3("ExodusRenderToneAdvOptA", LLVector3(data["ToneAdvOptA"]));
	}
	else
	{
		gSavedSettings.getControl("ExodusRenderToneAdvOptA")->resetToDefault(true);
	}
	if(data.has("ToneAdvOptB"))
	{
		gSavedSettings.setVector3("ExodusRenderToneAdvOptB", LLVector3(data["ToneAdvOptB"]));
	}
	else
	{
		gSavedSettings.getControl("ExodusRenderToneAdvOptB")->resetToDefault(true);
	}
	if(data.has("ToneAdvOptC"))
	{
		gSavedSettings.setVector3("ExodusRenderToneAdvOptC", LLVector3(data["ToneAdvOptC"]));
	}
	else
	{
		gSavedSettings.getControl("ExodusRenderToneAdvOptC")->resetToDefault(true);
	}
	updateUI();
}

/* ---------------------------------------------------------------------------- */
/* IMPORTING PRESETS															*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::importFile()
{
	LLFilePicker& file_picker = LLFilePicker::instance();
	if (file_picker.getOpenFile(LLFilePicker::FFLOAD_EVON_AG))
	{
		LLSD data;
		std::string filename = file_picker.getFirstFile();
		llifstream file(file_picker.getFirstFile());
		if (file.is_open() && evonManager::fromEVON(data, file, EVON_TYPE, EVON_VERSION_NUM) > 0)
		{
			file.close();
			loadPreset(data);
			std::string name = gDirUtilp->getBaseFileName(filename, true, true);
			
			std::string label = name + "*";
			loadPreset(data);
			LLComboBox* presetCombo = getChild<LLComboBox>("PresetsCombo");
			if (!presetCombo->itemExists(label))
			{
				presetCombo->add(label);
				presetCombo->selectByValue(LLSD(label));
				savePresetFinal(name);
				gSavedSettings.setString("ExodusAdvancedGraphicsPreset", label);
				updateButtons(true);
			}
			else
			{
				presetCombo->clear();
			}
		}
		else
		{
			llwarns << "Unable to import data!" << llendl;
		}
	}
}

/* ---------------------------------------------------------------------------- */
/* IMPORTING PRESETS FROM NOTECARD												*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::importNotecard(const LLInventoryItem* item)
{
	if (item)
	{
		loadNotecardName = item->getName();

		LLSD args, payload;
		args["NAME"] = loadNotecardName;
		payload["owner-key"] = item->getPermissions().getOwner();
		payload["item-key"] = item->getUUID();
		payload["asset-key"] = item->getAssetUUID();
		payload["type"] = item->getType();

		LLNotificationsUtil::add("ExodusAdvancedGraphicsImport", args, payload, boost::bind(importNotecardCallback, _1, _2));
	}
}

void exoFloaterAdvancedGraphics::importNotecardCallback(const LLSD& notification, const LLSD& response)
{
	if (LLNotificationsUtil::getSelectedOption(notification, response) == 0)
	{
		exoFloaterAdvancedGraphics* self =
			LLFloaterReg::getTypedInstance<exoFloaterAdvancedGraphics>("exo_advanced_graphics");
		if (self)
		{
			if (!self->loadNotecardName.empty())
			{
				LLUUID key = notification["payload"]["asset-key"].asUUID();
				LLUUID* id = new LLUUID(key);
				const LLHost source = LLHost::invalid;

				gAssetStorage->getInvItemAsset(
					source, gAgent.getID(), gAgent.getSessionID(),
					notification["payload"]["owner-key"].asUUID(), LLUUID::null,
					notification["payload"]["item-key"].asUUID(), key,
					(LLAssetType::EType)notification["payload"]["type"].asInteger(),
					&self->loadNotecard, (void*)id, TRUE
				);
			}
		}
	}
}

void exoFloaterAdvancedGraphics::loadNotecard(LLVFS* vfs, const LLUUID& id, LLAssetType::EType type, void* userdata, S32 status, LLExtStat ext)
{
	exoFloaterAdvancedGraphics* self =
		LLFloaterReg::getTypedInstance<exoFloaterAdvancedGraphics>("exo_advanced_graphics");
	if (self)
	{
		if (status == LL_ERR_NOERR && !self->loadNotecardName.empty())
		{
			S32 size = vfs->getSize(id, type);
			char* buffer = new char[size];
			vfs->getData(id, type, (U8*)buffer, 0, size);
			if (buffer)
			{
				/**
				 * We need to scrub the notecard data such as length, etc.
				 * Let's get rid of everything before the hash symbol.
				 * The first line should be "# EVON ..." The last line has an extra "}".
				 * Also, there seems to be always be 79 extra characters at the end, not sure why?
				 */

				std::string text(buffer);
				delete buffer;
				std::size_t start = text.find_first_of('#');
				if (start != std::string::npos && start != 0)
				{
					LLSD data;
					std::stringstream notecard(text.substr(start, text.length()));
					std::string versionData, notecardData;
					std::getline(notecard, versionData);
					std::getline(notecard, notecardData);
					std::stringstream final(versionData + "\n" + notecardData);
					if (evonManager::fromEVON(data, final, EVON_TYPE, EVON_VERSION_NUM) > 0)
					{
						std::string name = gDirUtilp->getBaseFileName(self->loadNotecardName, true, true);
						
						llinfos << "Loaded notecard \"" << self->loadNotecardName << "\"." << llendl;
						self->loadNotecardName.erase();
						self->loadPreset(data);
						
						LLComboBox* presetCombo = self->getChild<LLComboBox>("PresetsCombo");
						if (presetCombo)
						{
							std::string label = name + "*";
							if (!presetCombo->itemExists(label))
							{
								presetCombo->add(label);
								presetCombo->selectByValue(LLSD(label));
								self->savePresetFinal(name);
								gSavedSettings.setString("ExodusAdvancedGraphicsPreset", label);
								self->updateButtons(true);
							}
							else
							{
								presetCombo->clear();
								self->updateButtons(false);
							}
						}
						return;
					}
				}

				llwarns << "Unable to phrase \"" << self->loadNotecardName << "\", aborting import." << llendl;
				self->loadNotecardName.erase();
				LLNotificationsUtil::add("ExodusAdvancedGraphicsFailed", LLSD());
				return;
			}
		}

		llwarns << "Download failed for \"" << self->loadNotecardName << "\", aborting import." << llendl;
		self->loadNotecardName.erase();
	}
	else
	{
		llwarns << "Unable to find preset manager, cannot import notecard." << llendl;
	}

	LLNotificationsUtil::add("ExodusAdvancedGraphicsFailed", LLSD());
}

/* ---------------------------------------------------------------------------- */
/* EXPORTING PRESETS															*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::exportData()
{
	std::string filename = getChild<LLComboBox>("PresetsCombo")->getSelectedItemLabel();
	if (filename.length() > 1 &&
		filename[filename.length() - 1] == '*')
	{
		filename = filename.substr(0, filename.length() - 1);

		LLSD args, payload;
		args["NAME"] = filename;
		payload["filename"] = filename;

		LLNotificationsUtil::add("ExodusAdvancedGraphicsExport", args, payload, boost::bind(exportDataCallback, _1, _2));
	}
}

void exoFloaterAdvancedGraphics::exportDataCallback(const LLSD& notification, const LLSD& response)
{
	S32 selection = LLNotificationsUtil::getSelectedOption(notification, response);
	if (selection < 2)
	{
		std::string filename = notification["payload"]["filename"].asString();
		if (filename.empty()) return;

		exoFloaterAdvancedGraphics* self =
			LLFloaterReg::getTypedInstance<exoFloaterAdvancedGraphics>("exo_advanced_graphics");
		if (self)
		{
			if (selection) self->exportNotecard(filename);
			else self->exportFile(filename);
		}
	}
}

void exoFloaterAdvancedGraphics::exportFile(std::string filename)
{
	LLFilePicker& file_picker = LLFilePicker::instance();
	if (file_picker.getSaveFile(LLFilePicker::FFSAVE_EVON_AG, filename))
	{
		LLSD data = generateData();

		llofstream evon_file(file_picker.getFirstFile());
		if (evon_file.is_open())
		{
			evonManager::toPrettyEVON(data, evon_file, EVON_TYPE, EVON_VERSION);
			evon_file.close();
		}
		else
		{
			llwarns << "Unable to export data to:" << llendl;
			llinfos << file_picker.getFirstFile() << llendl;
		}
	}
	else llinfos << "Get save file returned false." << llendl;
}

/* ---------------------------------------------------------------------------- */
/* EXPORTING PRESETS TO A NOTECARD												*/
/* ---------------------------------------------------------------------------- */

class exoFloaterAdvancedGraphicsNotecardCallback :
	public LLInventoryCallback
{
private:
	std::string mNotecardData;
	LLUUID mInventoryFolder;
	
public:
	void set(LLUUID id, std::string data);
	void fire(const LLUUID& id);
};

void exoFloaterAdvancedGraphicsNotecardCallback::set(LLUUID id, std::string data)
{
	mInventoryFolder = id;
	mNotecardData = data;
}

void exoFloaterAdvancedGraphics::exportNotecard(std::string name)
{
	const LLUUID folderID = gInventory.findCategoryByName(EXODUS_FOLDER);
	if (folderID.isNull())
	{
		llinfos << "Could not find \"" << EXODUS_FOLDER << "\" folder in inventory, creating..." << llendl;

		mInventoryFolder.setNull();
		gInventory.createNewCategory(gInventory.getRootFolderID(), LLFolderType::FT_NONE, EXODUS_FOLDER);
	}
	else if (mInventoryFolder.isNull())
	{
		LLInventoryModel::cat_array_t* categoryFolders;
		LLInventoryModel::item_array_t* categoryItems;
		gInventory.getDirectDescendentsOf(folderID, categoryFolders, categoryItems);
		for (int i = 0; i < categoryFolders->count(); ++i)
		{
			const std::string& folderName = categoryFolders->get(i)->getName();
			if (folderName.compare(PRESET_FOLDER) == 0)
			{
				mInventoryFolder = categoryFolders->get(i)->getUUID();
				break;
			}
		}

		if (mInventoryFolder.isNull())
		{
			llinfos << "Could not find \"" << PRESET_FOLDER << "\" in \"" <<
				EXODUS_FOLDER << "\" folder in inventory, creating..." << llendl;

			mInventoryFolder =
				gInventory.createNewCategory(folderID, LLFolderType::FT_NONE, PRESET_FOLDER);
		}

		if (mInventoryFolder.isNull())
		{
			llwarns << "Unable to create \"" << PRESET_FOLDER << "\" folder!" << llendl;
			
			LLSD args;
			args["MESSAGE"] = "Export failed, was unable to create the preset folder in your inventory.";
			LLNotificationsUtil::add("SystemMessageTip", args);

			return;
		}
	}

	llinfos << "Exporting notecard to presets folder..." << llendl;

	LLSD dataToExport = generateData();

	std::stringstream dataToSave;
	evonManager::toEVON(dataToExport, dataToSave, EVON_TYPE, EVON_VERSION);

	std::string finalNotecard = dataToSave.str();
	if ((finalNotecard.length() * sizeof(std::string::value_type)) <= 65536)
	{
		llinfos << "Attempting to create notecard..." << llendl;

		LLPointer<exoFloaterAdvancedGraphicsNotecardCallback> callback = new exoFloaterAdvancedGraphicsNotecardCallback();

		callback->set(mInventoryFolder, finalNotecard);

		create_inventory_item(
			gAgent.getID(), gAgent.getSessionID(),
			mInventoryFolder, LLTransactionID::tnull,
			name + EVON_EXTENTION, "Advanced Graphics Preset",
			LLAssetType::AT_NOTECARD, LLInventoryType::IT_NOTECARD,
			NOT_WEARABLE, PERM_ITEM_UNRESTRICTED,
			callback
		);
	}
	else
	{
		llwarns << "Notecard exceeds maximum length! Failed to export notecard." << llendl;
	}
}

void exoFloaterAdvancedGraphicsNotecardCallback::fire(const LLUUID& id)
{
	if (mNotecardData.empty()) return;
	
	LLInventoryItem *item = gInventory.getItem(id);
	if (item)
	{
		LLNotecard notecardAsset;
		notecardAsset.setText(mNotecardData);

		const LLViewerRegion* region = gAgent.getRegion();
		if (!region)
		{
			llwarns << "Not connected to a region, cannot save notecard." << llendl;
			return;
		}

		std::string agent_url = region->getCapability("UpdateNotecardAgentInventory");
		if (!agent_url.empty())
		{
			llinfos << "Saving notecard via " << agent_url << "." << llendl;
			
			LLAssetID asset_id;
			LLTransactionID tid;
			tid.generate();
			asset_id = tid.makeAssetID(gAgent.getSecureSessionID());

			LLVFile file(gVFS, asset_id, LLAssetType::AT_NOTECARD, LLVFile::APPEND);

			std::ostringstream stringStream;
			notecardAsset.exportStream(stringStream);
			std::string buffer = stringStream.str();
			S32 size = buffer.length() + 1;

			file.setMaxSize(size);
			file.write((U8*)buffer.c_str(), size);

			LLSD body;
			body["item_id"] = id;
			LLHTTPClient::post(agent_url, body, new LLUpdateAgentInventoryResponder(body, asset_id, LLAssetType::AT_NOTECARD));
		}
		else // Todo: Maybe put it into the trash?
		{
			LLSD args;
			args["MESSAGE"] = "Export failed, was unable to save the preset.";
			LLNotificationsUtil::add("SystemMessageTip", args);

			llwarns << "Unable to save preset data to the notecard." << llendl;
		}
	}
	else
	{
		llwarns << "Unable to find created notecard in inventory, oh god." << llendl;
	}
}

/* ---------------------------------------------------------------------------- */
/* GENERATING PRESET LLSD														*/
/* ---------------------------------------------------------------------------- */

LLSD exoFloaterAdvancedGraphics::generateData()
{
	LLSD data;

	data["Gamma"] = gSavedSettings.getVector3("ExodusRenderGamma").getValue();
	data["Exposure"] = gSavedSettings.getVector3("ExodusRenderExposure").getValue();
	data["Offset"] = gSavedSettings.getVector3("ExodusRenderOffset").getValue();
	data["Vignette"] = gSavedSettings.getVector3("ExodusRenderVignette").getValue();
	data["ToneMappingTech"] = gSavedSettings.getS32("ExodusRenderToneMappingTech");
	data["ToneExposure"] = gSavedSettings.getF32("ExodusRenderToneExposure");
	data["ToneAdvOptA"]= gSavedSettings.getVector3("ExodusRenderToneAdvOptA").getValue();
	data["ToneAdvOptB"] = gSavedSettings.getVector3("ExodusRenderToneAdvOptB").getValue();
	data["ToneAdvOptC"] = gSavedSettings.getVector3("ExodusRenderToneAdvOptC").getValue();

	return data;
}

/* ---------------------------------------------------------------------------- */
/* GAMMA UI CALLBACKS															*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::onAdjustGammaRed()
{
	LLVector3 gamma = gSavedSettings.getVector3("ExodusRenderGamma");
	gamma.mV[VX] = getChild<LLSlider>("GammaRed")->getValueF32();
	getChild<LLSpinCtrl>("GammaRedSpinner")->setValue(gamma.mV[VX]);

	gSavedSettings.setVector3("ExodusRenderGamma", gamma);
}

void exoFloaterAdvancedGraphics::onAdjustGammaGreen()
{
	LLVector3 gamma = gSavedSettings.getVector3("ExodusRenderGamma");
	gamma.mV[VY] = getChild<LLSlider>("GammaGreen")->getValueF32();
	getChild<LLSpinCtrl>("GammaGreenSpinner")->setValue(gamma.mV[VY]);

	gSavedSettings.setVector3("ExodusRenderGamma", gamma);
}

void exoFloaterAdvancedGraphics::onAdjustGammaBlue()
{
	LLVector3 gamma = gSavedSettings.getVector3("ExodusRenderGamma");
	gamma.mV[VZ] = getChild<LLSlider>("GammaBlue")->getValueF32();
	getChild<LLSpinCtrl>("GammaBlueSpinner")->setValue(gamma.mV[VZ]);

	gSavedSettings.setVector3("ExodusRenderGamma", gamma);
}

void exoFloaterAdvancedGraphics::onAdjustGammaRedSpinner()
{
	LLVector3 gamma = gSavedSettings.getVector3("ExodusRenderGamma");
	gamma.mV[VX] = getChild<LLSpinCtrl>("GammaRedSpinner")->getValueF32();
	getChild<LLSlider>("GammaRed")->setValue(gamma.mV[VX]);

	gSavedSettings.setVector3("ExodusRenderGamma", gamma);
}

void exoFloaterAdvancedGraphics::onAdjustGammaGreenSpinner()
{
	LLVector3 gamma = gSavedSettings.getVector3("ExodusRenderGamma");
	gamma.mV[VY] = getChild<LLSpinCtrl>("GammaGreenSpinner")->getValueF32();
	getChild<LLSlider>("GammaGreen")->setValue(gamma.mV[VY]);

	gSavedSettings.setVector3("ExodusRenderGamma", gamma);
}

void exoFloaterAdvancedGraphics::onAdjustGammaBlueSpinner()
{
	LLVector3 gamma = gSavedSettings.getVector3("ExodusRenderGamma");
	gamma.mV[VZ] = getChild<LLSpinCtrl>("GammaBlueSpinner")->getValueF32();
	getChild<LLSlider>("GammaBlue")->setValue(gamma.mV[VZ]);

	gSavedSettings.setVector3("ExodusRenderGamma", gamma);
}

/* ---------------------------------------------------------------------------- */
/* EXPOSURE UI CALLBACKS														*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::onAdjustExposureRed()
{
	LLVector3 exposure = gSavedSettings.getVector3("ExodusRenderExposure");
	exposure.mV[VX] = getChild<LLSlider>("ExposureRed")->getValueF32();
	getChild<LLSpinCtrl>("ExposureRedSpinner")->setValue(exposure.mV[VX]);

	gSavedSettings.setVector3("ExodusRenderExposure", exposure);
}

void exoFloaterAdvancedGraphics::onAdjustExposureGreen()
{
	LLVector3 exposure = gSavedSettings.getVector3("ExodusRenderExposure");
	exposure.mV[VY] = getChild<LLSlider>("ExposureGreen")->getValueF32();
	getChild<LLSpinCtrl>("ExposureGreenSpinner")->setValue(exposure.mV[VY]);

	gSavedSettings.setVector3("ExodusRenderExposure", exposure);
}

void exoFloaterAdvancedGraphics::onAdjustExposureBlue()
{
	LLVector3 exposure = gSavedSettings.getVector3("ExodusRenderExposure");
	exposure.mV[VZ] = getChild<LLSlider>("ExposureBlue")->getValueF32();
	getChild<LLSpinCtrl>("ExposureBlueSpinner")->setValue(exposure.mV[VZ]);

	gSavedSettings.setVector3("ExodusRenderExposure", exposure);
}

void exoFloaterAdvancedGraphics::onAdjustExposureRedSpinner()
{
	LLVector3 exposure = gSavedSettings.getVector3("ExodusRenderExposure");
	exposure.mV[VX] = getChild<LLSpinCtrl>("ExposureRedSpinner")->getValueF32();
	getChild<LLSlider>("ExposureRed")->setValue(exposure.mV[VX]);

	gSavedSettings.setVector3("ExodusRenderExposure", exposure);
}

void exoFloaterAdvancedGraphics::onAdjustExposureGreenSpinner()
{
	LLVector3 exposure = gSavedSettings.getVector3("ExodusRenderExposure");
	exposure.mV[VY] = getChild<LLSpinCtrl>("ExposureGreenSpinner")->getValueF32();
	getChild<LLSlider>("ExposureGreen")->setValue(exposure.mV[VY]);

	gSavedSettings.setVector3("ExodusRenderExposure", exposure);
}

void exoFloaterAdvancedGraphics::onAdjustExposureBlueSpinner()
{
	LLVector3 exposure = gSavedSettings.getVector3("ExodusRenderExposure");
	exposure.mV[VZ] = getChild<LLSpinCtrl>("ExposureBlueSpinner")->getValueF32();
	getChild<LLSlider>("ExposureBlue")->setValue(exposure.mV[VZ]);

	gSavedSettings.setVector3("ExodusRenderExposure", exposure);
}

/* ---------------------------------------------------------------------------- */
/* OFFSET UI CALLBACKS															*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::onAdjustOffsetRed()
{
	LLVector3 offset = gSavedSettings.getVector3("ExodusRenderOffset");
	offset.mV[VX] = getChild<LLSlider>("OffsetRed")->getValueF32();
	getChild<LLSpinCtrl>("OffsetRedSpinner")->setValue(offset.mV[VX]);

	gSavedSettings.setVector3("ExodusRenderOffset", offset);
}

void exoFloaterAdvancedGraphics::onAdjustOffsetGreen()
{
	LLVector3 offset = gSavedSettings.getVector3("ExodusRenderOffset");
	offset.mV[VY] = getChild<LLSlider>("OffsetGreen")->getValueF32();
	getChild<LLSpinCtrl>("OffsetGreenSpinner")->setValue(offset.mV[VY]);

	gSavedSettings.setVector3("ExodusRenderOffset", offset);
}

void exoFloaterAdvancedGraphics::onAdjustOffsetBlue()
{
	LLVector3 offset = gSavedSettings.getVector3("ExodusRenderOffset");
	offset.mV[VZ] = getChild<LLSlider>("OffsetBlue")->getValueF32();
	getChild<LLSpinCtrl>("OffsetBlueSpinner")->setValue(offset.mV[VZ]);

	gSavedSettings.setVector3("ExodusRenderOffset", offset);
}

void exoFloaterAdvancedGraphics::onAdjustOffsetRedSpinner()
{
	LLVector3 offset = gSavedSettings.getVector3("ExodusRenderOffset");
	offset.mV[VX] = getChild<LLSpinCtrl>("OffsetRedSpinner")->getValueF32();
	getChild<LLSlider>("OffsetRed")->setValue(offset.mV[VX]);

	gSavedSettings.setVector3("ExodusRenderOffset", offset);
}

void exoFloaterAdvancedGraphics::onAdjustOffsetGreenSpinner()
{
	LLVector3 offset = gSavedSettings.getVector3("ExodusRenderOffset");
	offset.mV[VY] = getChild<LLSpinCtrl>("OffsetGreenSpinner")->getValueF32();
	getChild<LLSlider>("OffsetGreen")->setValue(offset.mV[VY]);

	gSavedSettings.setVector3("ExodusRenderOffset", offset);
}

void exoFloaterAdvancedGraphics::onAdjustOffsetBlueSpinner()
{
	LLVector3 offset = gSavedSettings.getVector3("ExodusRenderOffset");
	offset.mV[VZ] = getChild<LLSpinCtrl>("OffsetBlueSpinner")->getValueF32();
	getChild<LLSlider>("OffsetBlue")->setValue(offset.mV[VZ]);

	gSavedSettings.setVector3("ExodusRenderOffset", offset);
}

/* ---------------------------------------------------------------------------- */
/* VIGNETTE UI CALLBACKS														*/
/* ---------------------------------------------------------------------------- */

void exoFloaterAdvancedGraphics::onAdjustVignetteX()
{
	LLVector3 vignette = gSavedSettings.getVector3("ExodusRenderVignette");
	vignette.mV[VX] = getChild<LLSlider>("VignetteSliderX")->getValueF32();
	getChild<LLSpinCtrl>("VignetteSpinnerX")->setValue(vignette.mV[VX]);

	gSavedSettings.setVector3("ExodusRenderVignette", vignette);
}

void exoFloaterAdvancedGraphics::onAdjustVignetteY()
{
	LLVector3 vignette = gSavedSettings.getVector3("ExodusRenderVignette");
	vignette.mV[VY] = getChild<LLSlider>("VignetteSliderY")->getValueF32();
	getChild<LLSpinCtrl>("VignetteSpinnerY")->setValue(vignette.mV[VY]);

	gSavedSettings.setVector3("ExodusRenderVignette", vignette);
}

void exoFloaterAdvancedGraphics::onAdjustVignetteZ()
{
	LLVector3 vignette = gSavedSettings.getVector3("ExodusRenderVignette");
	vignette.mV[VZ] = getChild<LLSlider>("VignetteSliderZ")->getValueF32();
	getChild<LLSpinCtrl>("VignetteSpinnerZ")->setValue(vignette.mV[VZ]);

	gSavedSettings.setVector3("ExodusRenderVignette", vignette);
}

void exoFloaterAdvancedGraphics::onAdjustVignetteSpinnerX()
{
	LLVector3 vignette = gSavedSettings.getVector3("ExodusRenderVignette");
	vignette.mV[VX] = getChild<LLSpinCtrl>("VignetteSpinnerX")->getValueF32();
	getChild<LLSlider>("VignetteSliderX")->setValue(vignette.mV[VX]);

	gSavedSettings.setVector3("ExodusRenderVignette", vignette);
}

void exoFloaterAdvancedGraphics::onAdjustVignetteSpinnerY()
{
	LLVector3 vignette = gSavedSettings.getVector3("ExodusRenderVignette");
	vignette.mV[VY] = getChild<LLSpinCtrl>("VignetteSpinnerY")->getValueF32();
	getChild<LLSlider>("VignetteSliderY")->setValue(vignette.mV[VY]);

	gSavedSettings.setVector3("ExodusRenderVignette", vignette);
}

void exoFloaterAdvancedGraphics::onAdjustVignetteSpinnerZ()
{
	LLVector3 vignette = gSavedSettings.getVector3("ExodusRenderVignette");
	vignette.mV[VZ] = getChild<LLSpinCtrl>("VignetteSpinnerZ")->getValueF32();
	getChild<LLSlider>("VignetteSliderZ")->setValue(vignette.mV[VZ]);

	gSavedSettings.setVector3("ExodusRenderVignette", vignette);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneShoulderStr()
{
	std::string setting = "ExodusRenderToneAdvOptA";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VX] = getChild<LLSlider>("ShoulderStrengthSlider")->getValueF32();
	getChild<LLSpinCtrl>("ShoulderStrengthSpinner")->setValue(tone.mV[VX]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneShoulderStrSpinner()
{
	std::string setting = "ExodusRenderToneAdvOptA";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VX] = getChild<LLSpinCtrl>("ShoulderStrengthSpinner")->getValueF32();
	getChild<LLSlider>("ShoulderStrengthSpinner")->setValue(tone.mV[VX]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneLinearStr()
{
	std::string setting = "ExodusRenderToneAdvOptA";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VY] = getChild<LLSlider>("LinearStrengthSlider")->getValueF32();
	getChild<LLSpinCtrl>("LinearStrengthSpinner")->setValue(tone.mV[VY]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneLinearStrSpinner()
{
	std::string setting = "ExodusRenderToneAdvOptA";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VY] = getChild<LLSpinCtrl>("LinearStrengthSpinner")->getValueF32();
	getChild<LLSlider>("LinearStrengthSlider")->setValue(tone.mV[VY]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneLinearAngle()
{
	std::string setting = "ExodusRenderToneAdvOptA";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VZ] = getChild<LLSlider>("LinearAngleSlider")->getValueF32();
	getChild<LLSpinCtrl>("LinearAngleSpinner")->setValue(tone.mV[VZ]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneLinearAngleSpinner()
{
	std::string setting = "ExodusRenderToneAdvOptA";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VZ] = getChild<LLSpinCtrl>("LinearAngleSpinner")->getValueF32();
	getChild<LLSlider>("LinearAngleSlider")->setValue(tone.mV[VZ]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneToeStr()
{
	std::string setting = "ExodusRenderToneAdvOptB";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VX] = getChild<LLSlider>("ToeStrengthSlider")->getValueF32();
	getChild<LLSpinCtrl>("ToeStrengthSpinner")->setValue(tone.mV[VX]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneToeStrSpinner()
{
	std::string setting = "ExodusRenderToneAdvOptB";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VX] = getChild<LLSpinCtrl>("ToeStrengthSpinner")->getValueF32();
	getChild<LLSlider>("ToeStrengthSlider")->setValue(tone.mV[VX]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneToeNumerator()
{
	std::string setting = "ExodusRenderToneAdvOptB";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VY] = getChild<LLSlider>("ToeNumeratorSlider")->getValueF32();
	getChild<LLSpinCtrl>("ToeNumeratorSpinner")->setValue(tone.mV[VY]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneToeNumeratorSpinner()
{
	std::string setting = "ExodusRenderToneAdvOptB";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VY] = getChild<LLSpinCtrl>("ToeNumeratorSpinner")->getValueF32();
	getChild<LLSlider>("ToeNumeratorSlider")->setValue(tone.mV[VY]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneToeDenominator()
{
	std::string setting = "ExodusRenderToneAdvOptB";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VZ] = getChild<LLSlider>("ToeDenominatorSlider")->getValueF32();
	getChild<LLSpinCtrl>("ToeDenominatorSpinner")->setValue(tone.mV[VZ]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneToeDenominatorSpinner()
{
	std::string setting = "ExodusRenderToneAdvOptB";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VZ] = getChild<LLSpinCtrl>("ToeDenominatorSpinner")->getValueF32();
	getChild<LLSlider>("ToeDenominatorSlider")->setValue(tone.mV[VZ]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneLinearWhitePoint()
{
	std::string setting = "ExodusRenderToneAdvOptC";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VX] = getChild<LLSlider>("LinearWhitePointSlider")->getValueF32();
	getChild<LLSpinCtrl>("LinearWhitePointSpinner")->setValue(tone.mV[VX]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneLinearWhitePointSpinner()
{
	std::string setting = "ExodusRenderToneAdvOptC";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VX] = getChild<LLSpinCtrl>("LinearWhitePointSpinner")->getValueF32();
	getChild<LLSlider>("LinearWhitePointSlider")->setValue(tone.mV[VX]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneExposureBias()
{
	std::string setting = "ExodusRenderToneAdvOptC";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VY] = getChild<LLSlider>("ExposureBiasSlider")->getValueF32();
	getChild<LLSpinCtrl>("ExposureBiasSpinner")->setValue(tone.mV[VY]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onAdjustAdvToneExposureBiasSpinner()
{
	std::string setting = "ExodusRenderToneAdvOptC";
	LLVector3 tone = gSavedSettings.getVector3(setting);
	tone.mV[VY] = getChild<LLSpinCtrl>("ExposureBiasSpinner")->getValueF32();
	getChild<LLSlider>("ExposureBiasSlider")->setValue(tone.mV[VY]);
	
	gSavedSettings.setVector3(setting, tone);
}

void exoFloaterAdvancedGraphics::onSetToneMappingMode()
{
	
}

/* ---------------------------------------------------------------------------- */
/* END																			*/
/* ---------------------------------------------------------------------------- */
