/** 
 * @file exobridge.h
 * @brief exoBridge class definitions
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011, Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_API
#define EXO_API

// Command prefixes:
#define GENESIS_CMD_PREFIX		'#'

// Other non-included classes:
class LLViewerObject;

// Main class definition:
class eAPI //: public LLEventTimer
{
public:
	static BOOL phraseChat(std::string message, LLViewerObject* chatter, LLUUID from_id);
	static void draw();

private:
	//virtual BOOL tick();
};

#endif // EXO_API
